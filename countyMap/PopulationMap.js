var width = 960,
height = 540;

var path = d3.geo.path();

var svg = d3.select("#visual").append("svg")
.attr("width", width)
.attr("height", height);

//Setup Tooltip 
var statetooltip = d3.select("#visual")
.append("div")
.style("position", "absolute")
.style("z-index", "10")
.style("visibility", "hidden")
.style("color", "white")
.style("padding", "10px")
.style("background", "black")
.style("border-color", "black")
.style("border-radius", "10px")
.style("border-style", "solid");

d3.json("countyMap/json/us.json", function(error, topology) {
	if (error) throw error;

	d3.json("countyMap/json/census.json", function(error, jsonData) {
		if (error) throw error;

		//Put data into the correct format 
		var data = [];
		jsonData.forEach(function(d, i){
			if(i != 0)
			{
				data.push({
					countyId : (Number(d[2])*1000)+Number(d[3]),
					data : Number(d[0])

				});
			}

		});

		//Create log scale
		var max_pop = d3.max( data, function(d) { return d.data});
		var min_pop = d3.min( data, function(d) { return d.data});
		var color_scale = d3.scale.linear()
		.domain([Math.log(min_pop), Math.log(max_pop)])
		.range(['white','red']);

		//create map 
		svg.selectAll("path")
		.data(matchCountiesAndTopology(topojson.feature(topology, topology.objects.counties).features,data))
		.enter().append("path")
		.style('stroke','black')
		.style('stroke-width','.5px')
		.style('fill', function(d) {
			return color_scale(Math.log(d.data));
		})
		.attr("d", path)
		.on("mouseover", function(d){
			d3.select(this)
			.style("stroke-width","2px")
			.moveToFront();

			return statetooltip
			.style("visibility", "visible")
			.html(function() { 
				return "<p>"+d.county+", "+d.abbreviation+"</p>"
				+"<p>Population: "+NumberToString(d.data)+"</p>";
			});
		})
		.on("mousemove", function(){
			return statetooltip.style("top", (event.pageY-60)+"px")
			.style("left",(event.pageX)+"px");
		})
		.on("mouseout", function(){
			d3.select(this).style("stroke-width",".5px");
			return statetooltip.style("visibility", "hidden");
		});

		renderScale(max_pop, min_pop, "red", "white");
		
	});
});

//function that creates color scale 
function renderScale(max, min, maxColor, minColor)
{
	var gradient = svg.append("defs")
	.append("linearGradient")
	.attr("id", "gradient")
	.attr("x1", "0%")
	.attr("y1", "0%")
	.attr("x2", "0%")
	.attr("y2", "100%");

	gradient.append("stop")
	.attr("offset", "0%")
	.attr("stop-color", minColor)
	.attr("stop-opacity", 1);

	gradient.append("stop")
	.attr("offset", "100%")
	.attr("stop-color", maxColor)
	.attr("stop-opacity", 1);

	svg.append("rect")
	.attr("width", 20)
	.attr("height", 300)
	.style("fill", "url(#gradient)");

	svg.append("text")
	.attr("x", "25")
	.attr("y", "12")
	.style("font-size","12px")
	.style("fill", "black")
	.text(NumberToString(min));  

	svg.append("text")
	.attr("x", "25")
	.attr("y", "300")
	.style("font-size","12px")
	.style("fill", "black")
	.text(NumberToString(max));   	  	
}

//function that adds commas to number 
function NumberToString(string)
{
	return string.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//Prototype that moves selected item to front 
d3.selection.prototype.moveToFront = function() {
	return this.each(function(){
		this.parentNode.appendChild(this);
	});
};



