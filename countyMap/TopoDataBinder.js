//combines topology and county data with user data 
function matchCountiesAndTopology(topology, data)
{	
	var hash = new Array(60000);

	topology.forEach(function(d, i){
		hash[d.id] = d;
	});  
	
	jsonCounties.forEach(function(d, i){ 
		
		var item = hash[d.id];
		item.state = d.state;
		item.abbreviation = d.abbreviation;
		item.county = d.county;  

	});

	data.forEach(function(d, i){
		
		if(d.countyId < hash.length && hash[d.countyId] != undefined)
		{
			hash[d.countyId].data = d.data;
		}

	});  

	return topology;
}


//combines topology and state data with user data 
function matchStatesAndTopology(topology, data)
{
	topology.forEach(function(d, i){ 
		if((d.id-1) < jsonState.length)
		{
			d.state = jsonState[d.id-1].name;
			d.abbreviation = jsonState[d.id-1].abbreviation
		}
	});
	
	data.forEach(function(dataD, dataI){ 
    	for (var j = 0; j < topology.length ; j++) {
    		var topD = topology[j];

    		if(dataD.name === topD.state)
    		{
    			topD.data = dataD.data;
    			break;
    		}
    	}
    });

	return topology;
}

