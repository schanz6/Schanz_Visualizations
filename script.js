

$(document).ready(function() {

    var pages = {};
    var page;
   $.get('config.json', function(config) {

        //Hashes config table 
        $.each(config,function( key, value ) {
    
            if(key != 0)
            {
              value.previous = config[key-1].path;
            }
            else
            {
              value.previous = null;
            }

            if(key < (config.length-1))
            {
              value.next = config[key+1].path;
            }
            else
            {
              value.next = null;
            }

            pages[value.path] = value; 

         }); 

          //load first window 
          var path = window.location.href.split('/');
          loadPage(pages[path[path.length-1]]);         
        
    }, 'json');
 
  $(window).on('hashchange', function(e){
      
      clearPage(page);

      var path = window.location.href.split('/');
      page = pages[path[path.length-1]];
      
      if(page == undefined)
      {
        $('#title').text("Page Not Found");
      }
      else
      {
        loadPage(page); 
      }
      
  });

  
});

function clearPage(oldPath)
{
  //clear old data 
  $('#code_block').empty();
  $('.tabs').empty();
  $('#visual').empty();
  $('#include').empty();  

  if(oldPath != undefined)
  {
     $.each(oldPath.scriptHide,function( key, value ) {
        
      removeJsFile(value+".js");
        
    });

     $.each(oldPath.scriptShow,function( key, value ) {
        
      removeJsFile(value+".js");
        
    });
  }

}

function removeJsFile(filename)
{
 var targetelement = "script";
 var targetattr = "src"
 var allsuspects=document.getElementsByTagName(targetelement)
 for (var i=allsuspects.length; i>=0; i--)
 { //search backwards within nodelist for matching elements to remove
  if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
   allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
 }
}  

function loadPage(page)
{
  //set topbar values 
  $('#title').text(page.title);

  if(page.previous != null)
  {
      $('#previous').attr("href", "#/"+page.previous).css('visibility', 'visible');;
  }
  else
  {
     $('#previous').css('visibility', 'hidden');
  }

  if(page.next != null)
  {
       $('#next').attr("href", "#/"+page.next).css('visibility', 'visible');;
  }
  else
  {
     $('#next').css('visibility', 'hidden');
  }

  //loads package content 
  $.get(page.path+'/packagesList.txt', function(data) {
         
         $('#include').text(data);  
         $('#include').each(function(i, e) {hljs.highlightBlock(e)});

    }, 'text');

  //add undisplayed script dynamically 
  $.each(page.scriptHide,function( key, value ) {
        
        var s = document.createElement("script");
         s.type = "text/javascript";
         s.src = page.path+"/"+value+".js";
         
         $("head").append(s);

         //console.loadPage

  });

  //add displayed scripts dynamically and adds text to content window  
  $.each(page.scriptShow,function( key, value ) {
        
      var s = document.createElement("script");
       s.type = "text/javascript";
       s.src = page.path+"/"+value+".js";
      
       $("head").prepend(s);

      $('.tabs').prepend('<li> <a href="#'+value+'-block">'+value+'.js</a></li>');
      $('#code_block').prepend('<div id="'+value+'-block"><pre><code id="'+value+'-code" class="js"></code></pre></div>')
      
      $.get(page.path+"/"+value+'.js', function(data) {

         $('#'+value+'-code').text(data);  
         $('#'+value+'-code').each(function(i, e) {hljs.highlightBlock(e)});

    }, 'text');
  });
  


    $('ul.tabs').each(function(){
      // For each set of tabs, we want to keep track of
      // which tab is active and its associated content
      var active, content, links = $(this).find('a');

      // If the location.hash matches one of the links, use that as the active tab.
      // If no match is found, use the first link as the initial active tab.
      active = $(links.filter('[href="'+location.hash+'"]')[0] || links[0]);
      
      active.addClass('active');

      content = $(active[0].hash);

      // Hide the remaining content
      links.not(active).each(function () {
        $(this.hash).hide();
      });

      // Bind the click event handler
      $(this).unbind('click').on('click', 'a', function(e){
        // Make the old tab inactive.
        active.removeClass('active');
        
        content.hide();

        // Update the variables with the new link and content
        active = $(this);
        content = $(this.hash);

        // Make the tab active.
        active.addClass('active');

        content.show();

        // Prevent the anchor's default click action
        e.preventDefault();
      });
    });

}