
var Converter = (function(my) {

	my.RawImageToBitMap = function( raw, width, height, depth ) {
	  
	  var offset, data, image, arr;

	  arr = raw;

	  offset = depth <= 8 ? 54 + Math.pow(2, depth)*4 : 54;
	  
	  //BMP Header
	  data  = 'BM';                                  // ID field
	  data += ValueTo4byte(offset + arr.length);     // BMP size
	  data += ValueTo4byte(0);                       // unused
	  data += ValueTo4byte(offset);                  // pixel data offset
	  
	  //DIB Header
	  data += ValueTo4byte(40);                      // DIB header length
	  data += ValueTo4byte(width);                   // image height
	  data += ValueTo4byte(height);                  // image width
	  data += String.fromCharCode(1, 0);     		 // colour panes
	  data += String.fromCharCode(depth, 0); 		 // bits per pixel
	  data += ValueTo4byte(0);                       // compression method
	  data += ValueTo4byte(arr.length);              // size of the raw data
	  data += ValueTo4byte(2835);                    // horizontal print resolution
	  data += ValueTo4byte(2835);                    // vertical print resolution
	  data += ValueTo4byte(0);                       // colour palette, 0 == 2^n
	  data += ValueTo4byte(0);                       // important colours
	  
	  //Grayscale tables for bit depths <= 8
	  if (depth <= 8) {
	    data += conv(0);
	    
	    for (var s = Math.floor(255/(Math.pow(2, depth)-1)), i = s; i < 256; i += s)  {
	      data += ValueTo4byte(i + i*256 + i*65536);
	    }
	  }
	  
	  //Pixel data
	  //data += String.fromCharCode.apply(String, arr);
	 
		for(var i = height-1; i >= 0; i--){
			for(var j = 0; j< width; j++)
			{
				data += String.fromCharCode(arr[(i*width*4)+(j*4)+2]);
				data += String.fromCharCode(arr[(i*width*4)+(j*4)+1]);
				data += String.fromCharCode(arr[(i*width*4)+(j*4)+0]);
				data += String.fromCharCode(arr[(i*width*4)+(j*4)+3]);
			}
		}

	  return  btoa(data);
	}
	
	function ValueTo4byte(size) {
	    return String.fromCharCode(size&0xff, (size>>8)&0xff, (size>>16)&0xff, (size>>24)&0xff);
	 }

	 my.Base64ToArrayBuffer = function(base64) {
	    var binary_string =  window.atob(base64);
	    var len = binary_string.length;
	    var bytes = new Uint8Array( len );
	    for (var i = 0; i < len; i++)        {
	        bytes[i] = binary_string.charCodeAt(i);
	    }
	    return bytes.buffer;
	}

	my.Base64toHEX = function(base64) {

	  var raw = atob(base64);

	  var HEX = '';

	  for ( i = 0; i < raw.length; i++ ) {

	    var _hex = raw.charCodeAt(i).toString(16)

	    HEX += (_hex.length==2?_hex:'0'+_hex);

	  }
	  return HEX.toUpperCase();

	}

	my.Base64ToBinary = function(dataURI) {
	  var BASE64_MARKER = ';base64,';
	  var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
	  var base64 = dataURI.substring(base64Index);
	  var raw = window.atob(base64);
	  var rawLength = raw.length;
	  var array = new Uint8Array(new ArrayBuffer(rawLength));

	  for(i = 0; i < rawLength; i++) {
	    array[i] = raw.charCodeAt(i);
	  }
	  return array;
	}

	 my.Uint8ToString = function(u8a){
	  var CHUNK_SZ = 0x8000;
	  var c = [];
	  for (var i=0; i < u8a.length; i+=CHUNK_SZ) {
	    c.push(String.fromCharCode.apply(null, u8a.subarray(i, i+CHUNK_SZ)));
	  }
	  var join = c.join("");

	  return btoa(join);
	}

	 
	return my;

}(Converter|| {}));
