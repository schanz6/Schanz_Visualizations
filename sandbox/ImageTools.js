var ImageTools = (function(my) {

	
	my.square = function(image, height, width, start)
	{
	  var result = image;//new Uint8Array(image.length);
	  var downFactor = width*4;
	  var leftFactor = 4;
	  var size = 10;
	  //var start = 154240;

	  for(var i = 0; i < size; i++){
	    for(var j = 0; j < size; j++){
	        
	        var p = start+ (downFactor*i)+(leftFactor*j)
	        result[p] = 0;
	        result[p+1] = 0;
	        result[p+2] = 255;
	        result[p+3] = 0;
	      
	    }
	  }

	    return result;
	}

	my.outlineSquare = function(image, height, width, start)
	{
	  var result = image;
	  var downFactor = width*4;
	  var leftFactor = 4;
	  var size = 10;

	  for(var i = 0; i < size; i++){
	        
	        var top = start+ (leftFactor*i);
	        result[top] = 255;
	        result[top+1] = 255;
	        result[top+2] = 255;
	        result[top+3] = 255;

	        var bottom = start+ (downFactor*(size-1))+(leftFactor*i);
	        result[bottom] = 255;
	        result[bottom+1] = 255;
	        result[bottom+2] = 255;
	        result[bottom+3] = 255;

	        var left = start+ (downFactor*i);
	        result[left] = 255;
	        result[left+1] = 255;
	        result[left+2] = 255;
	        result[left+3] = 255;

	        var right = start+ (downFactor*i)+(leftFactor*(size-1));
	        result[right] = 255;
	        result[right+1] = 255;
	        result[right+2] = 255;
	        result[right+3] = 255;
	      
	    
	  }

	    return result;
	}


	my.closestPixle = function(image, height, width, base)
	{
	  var r = base.r;
	  var g = base.g;
	  var b = base.b;

	  var minIndex = 0;
	  var min = 1000;
	 
	  for(var i = 0; i < image.length; i = i+4){
	      var temp = Math.abs(image[i]-r)+ Math.abs(image[i+1]-g) + Math.abs(image[i+2]-b);

	      if(temp < min)
	      {
	        min = temp;
	        minIndex = i;
	      }
	    }

	    return minIndex;
	}

	my.diff = function(firstImage , secondImage)
	{
	  var result = new Uint8Array(firstImage.length);

	  for(var i = 0; i < firstImage.length; i = i+4){
	      
	      var total = Math.abs((firstImage[i]-secondImage[i])) +  Math.abs((firstImage[i+1]-secondImage[i+1])) 
	                +  Math.abs((firstImage[i+2]-secondImage[i+2])) +  Math.abs((firstImage[i+3]-secondImage[i+3]));
	      if(total > 75)
	      {
	        result[i] = 255;
	        result[i+1] = 255;
	        result[i+2] = 255;
	        result[i+3] = 255;
	        //console.log("helo");
	      }
	      else
	      {
	        result[i] = 0;
	        result[i+1] = 0;
	        result[i+2] = 0;
	        result[i+3] = 0;
	      }
	    }

	    return result;
	}


	my.averageBlock = function(image, height, width, size)
	{
	  var result = new Uint8Array(image.length/(size*size));
	  var resultIndex  = 0;
	  var downFactor = width*4;
	  var leftFactor = 4;
	  var averageLength = size;
	  var averageWidth = size;


	  for(var i = 0; i < image.length; i = i){

	  		var red = 0;
	  		var green = 0;
	  		var blue = 0;

	  		var redCount = 0;
	  		var greenCount = 0;
	  		var blueCount = 0;

	  		var stopWidth = 0

	  		for(var j = 0; j < averageWidth; j++)
	  		{
	  			if((i+(j*4)) % downFactor >= i % downFactor )
	  			{
		  			for(var k = 0; k < averageLength; k++)
			  		{
			  			if((i+ (k* downFactor)) < image.length)
			  			{
			  				red += image[i+(j*4)+(k*downFactor)];
			  				green += image[i+(j*4)+(k*downFactor)+1];
			  				blue += image[i+(j*4)+(k*downFactor)+2];
			  				//alert(green+" "+red+" "+blue);
			  				redCount++;
					  		greenCount++;
					  		blueCount++;
			  			}
			  		}
			  		stopWidth++;
		  		}
	  		}


			result[resultIndex++] = red/redCount;
			result[resultIndex++] = green/ greenCount;
			result[resultIndex++] = blue/blueCount;
			result[resultIndex++] = 255;

	  		

	  		if(stopWidth != averageWidth)
	  		{
	  			i = i+ (stopWidth * 4) + (downFactor* (averageLength-1));
	  		}
	  		else
	  		{
	  			i =  i + (stopWidth * 4); 
	  		}
	      
	   }

	    return result;
	}

	my.baseSelection = function(image, height, width, start,size)
	{
	  var result = new Uint8Array(image.length/(size*size));
	  var resultIndex  = 0;
	  var downFactor = width*4;
	  var leftFactor = 4;
	  var averageLength = size;
	  var averageWidth = size;


	  
  		var red = 0;
  		var green = 0;
  		var blue = 0;

  		var redCount = 0;
  		var greenCount = 0;
  		var blueCount = 0;

  		var stopWidth = 0

  		for(var j = 0; j < averageWidth; j++)
  		{
  			for(var k = 0; k < averageLength; k++)
	  		{
  			
  				red += image[start+(j*4)+(k*downFactor)];
  				green += image[start+(j*4)+(k*downFactor)+1];
  				blue += image[start+(j*4)+(k*downFactor)+2];
  				//alert(green+" "+red+" "+blue);
  				redCount++;
		  		greenCount++;
		  		blueCount++;
	  			
	  		}
		  		
  		}

  		return {
		 r: (red/redCount),
		 g: (green/ greenCount),
		 b: (blue/blueCount)
		};
		
	}



	my.averageBlockOutdated = function(image, height, width, size)
	{
	  var result = new Uint8Array(image.length);
	  
	  var downFactor = width*4;
	  var leftFactor = 4;
	  var averageLength = size;
	  var averageWidth = size;


	  for(var i = 0; i < image.length; i = i){

	  		var red = 0;
	  		var green = 0;
	  		var blue = 0;

	  		var redCount = 0;
	  		var greenCount = 0;
	  		var blueCount = 0;

	  		var stopWidth = 0

	  		for(var j = 0; j < averageWidth; j++)
	  		{
	  			if((i+(j*4)) % downFactor >= i % downFactor )
	  			{
		  			for(var k = 0; k < averageLength; k++)
			  		{
			  			if((i+ (k* downFactor)) < image.length)
			  			{
			  				red += image[i+(j*4)+(k*downFactor)];
			  				green += image[i+(j*4)+(k*downFactor)+1];
			  				blue += image[i+(j*4)+(k*downFactor)+2];
			  				//alert(green+" "+red+" "+blue);
			  				redCount++;
					  		greenCount++;
					  		blueCount++;
			  			}
			  		}
			  		stopWidth++;
		  		}
	  		}

	  		for(var j = 0; j < averageWidth; j++)
	  		{
	  			if((i+(j*4)) % downFactor >= i % downFactor )
	  			{
		  			for(var k = 0; k < averageLength; k++)
			  		{
			  			if((i+(k* downFactor)) < image.length)
			  			{
			  				result[i+(j*4)+(k*downFactor)] = red/redCount;
			  				result[i+(j*4)+(k*downFactor)+1] = green/ greenCount;
			  				result[i+(j*4)+(k*downFactor)+2] = blue/blueCount;
			  				result[i+(j*4)+(k*downFactor)+3] = 255;
						}
			  		}
			  		
		  		}
	  		}

	  		if(stopWidth != averageWidth)
	  		{
	  			i = i+ (stopWidth * 4) + (downFactor* (averageLength-1));
	  		}
	  		else
	  		{
	  			i =  i + (stopWidth * 4); 
	  		}
	      
	   }

	    return result;
	}

	my.simpleEdge = function(image, height, width)
	{
	  var result = new Uint8Array(image.length);
	  var downFactor = width*4;
	  var leftFactor = 4;

	  for(var i = 0; i < image.length; i = i+4){

	      var right = 0;
	      var down = 0;

	      if(i+ downFactor < image.length)
	        down = Math.abs((image[i]-image[i+downFactor])) +  Math.abs((image[i+1]-image[i+1+downFactor])) 
	                +  Math.abs((image[i+2]-image[i+2+downFactor])) +  Math.abs((image[i+3]-image[i+3+downFactor]));
	      if(i+leftFactor < image.length)
	        right = Math.abs((image[i]-image[i+leftFactor])) +  Math.abs((image[i+1]-image[i+1+leftFactor])) 
	                +  Math.abs((image[i+2]-image[i+2+leftFactor])) +  Math.abs((image[i+3]-image[i+3+leftFactor]));

	      if(right > 35 || down > 35)
	      {
	        result[i] = 255;
	        result[i+1] = 255;
	        result[i+2] = 255;
	        result[i+3] = 255;
	      }
	      else
	      {
	        result[i] = 0;
	        result[i+1] = 0;
	        result[i+2] = 0;
	        result[i+3] = 0;
	      }
	    }

	    return result;
	}


	my.decode = function(arr) {
	  
	  var decoder = new JpegImage();
	  decoder.parse(arr);

	  var image = {
	    width: decoder.width,
	    height: decoder.height,
	    data: 
	      new Uint8Array(decoder.width * decoder.height * 4) 
	  };

	  decoder.copyToImageData(image);
	 
	  return image;
	}

	return my;

}(ImageTools|| {}));
