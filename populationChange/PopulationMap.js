var width = 960,
height = 540;

var data = [];
var path = d3.geo.path();

var svg = d3.select("#visual").append("svg")
.attr("width", width)
.attr("height", height);

var dateOptions = [
{
	text:"2000 to 2010",
	start: "2000",
	end: "2010"
},
{
	text:"1990 to 2010",
	start: "1990",
	end: "2010"
} ,
{
	text:"1990 to 2000",
	start: "1990",
	end: "2000"
}];

//Setup Tooltip 
var statetooltip = d3.select("#visual")
.append("div")
.style("position", "absolute")
.style("z-index", "10")
.style("visibility", "hidden")
.style("color", "white")
.style("padding", "10px")
.style("background", "black")
.style("border-color", "black")
.style("border-radius", "10px")
.style("border-style", "solid");

//hold map data
svg.append("g")
.attr("class", "usa");

//Get data 
d3.json("populationChange/json/us.json", function(error, topology) {
	if (error) throw error;

	d3.json("populationChange/json/census2010.json", function(error, jsonData10) {
		if (error) throw error;

		d3.json("populationChange/json/census2000.json", function(error, jsonData00) {
			if (error) throw error;

			d3.json("populationChange/json/census1990.json", function(error, jsonData90) {
				if (error) throw error;

				//Put data into the correct format 
				var rawData = [];
				var formatData = function(d,i,year){

					if(i != 0)
					{
						rawData.push({
							countyId : (Number(d[1])*1000)+Number(d[2]),
							data : Number(d[0]),
							hash : year

						});
					}

				};

				jsonData10.forEach(function(d, i){
					formatData(d,i, "2010");
				});

				jsonData00.forEach(function(d, i){
					formatData(d,i, "2000");
				});

				jsonData90.forEach(function(d, i){
					formatData(d,i,"1990");
				});


				data = matchCountiesAndTopology(
					topojson.feature(topology, topology.objects.counties).features
					,rawData
					);

				renderMap(data,dateOptions[0].start,dateOptions[0].end);
				renderSlider()
				
				
			});
		});
	});
});

//renders map based on selected years
function renderMap(data,firstYear, secondYear)
{
	console.log(firstYear+" "+secondYear);
	svg.selectAll(".usa").remove();
	//Create log scale
	var max_pop = d3.max( data, function(d) { return percentChange(d,firstYear,secondYear); });
	var min_pop = d3.min( data, function(d) { return percentChange(d,firstYear,secondYear); });

	console.log(max_pop+" "+min_pop);

	var color_scale = d3.scale.linear()
	.domain([min_pop, 0,max_pop])
	.range(['blue','white','red']);

	//create map 
	svg.selectAll(".usa")
	.data(data)
	.enter().append("path")
	.attr("class","map")
	.style('stroke','black')
	.style('stroke-width','.5px')
	.style('fill', function(d) {
		if(d.data == undefined)
			return "black";
		return color_scale(percentChange(d,firstYear,secondYear));
	})
	.attr("d", path)
	.on("mouseover", function(d){
		d3.select(this)
		.style("stroke-width","2px")
		.moveToFront();

		return statetooltip
		.style("visibility", "visible")
		.html(function() { 
			var change = percentChange(d,firstYear,secondYear);
			var output = "<p>"+d.county+", "+d.abbreviation+"</p>";

			if(change == undefined)
			{
				return output+"<p>No Data</p>";
			}
			
			return output
			+"<p>Change: "+roundNumber(change)+"%</p>"
			+"<p>"+firstYear+" Population: "+numberToString(d.data[firstYear])+"</p>"
			+"<p>"+secondYear+" Population: "+numberToString(d.data[secondYear])+"</p>";

		});
	})
	.on("mousemove", function(){
		return statetooltip.style("top", (event.pageY-60)+"px")
		.style("left",(event.pageX)+"px");
	})
	.on("mouseout", function(){
		d3.select(this).style("stroke-width",".5px");
		return statetooltip.style("visibility", "hidden");
	});

	renderScale(min_pop, 0, max_pop, "blue", "white", "red");

}

//Renders selection slider
function renderSlider()
{
	var x = d3.scale.linear()
	.domain([0,2])
	.range([0, 200])
	.clamp(true);

	var brush = d3.svg.brush()
	.x(x)
	.extent([0, 0])
	.on("brush", function(){
		var value = brush.extent()[0];

	  if (d3.event.sourceEvent) { // not a programmatic event
	  	value = x.invert(d3.mouse(this)[0]);
	  	brush.extent([value, value]);
	  }

	  handle.attr("cx", x(value));
	  
	})
	.on("brushend", function(){
	  if (!d3.event.sourceEvent) return; // only transition after input


	  var value = Math.round( brush.extent()[0]); 
	  console.log(value);

	  d3.select(this)
	  .transition() 
	  .duration(0)
	  .call(brush.extent([value, value]))
	  .call(brush.event);

	  renderMap(data,dateOptions[value].start,dateOptions[value].end); 
	});


	svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate("+(width/2-100)+","+ (0) + ")")
	.call(d3.svg.axis()
		.scale(x)
		.ticks(2)
		.orient("bottom")
		.tickFormat(function(d) { return dateOptions[d].text; })
		.tickSize(1)
		.tickPadding(12))
	.attr("class", "halo");

	var slider = svg.append("g")
	.attr("transform", "translate("+(width/2-100)+"," + (0) + ")")
	.attr("class", "slider")
	.attr("hight","50px")
	.call(brush);

	slider.selectAll(".extent,.resize")
	.remove();

	slider.select(".background")
	.attr("height", 30);


	var handle = slider.append("circle")
	.attr("class", "handle")
	.attr("r", 9);

}

//function that creates color scale 
function renderScale(min, mid, max, minColor, midColor ,maxColor)
{
	d3.selectAll(".scale").remove();
	var gradient = svg.append("defs")
	.append("linearGradient")
	.attr("id", "gradient")
	.attr("x1", "0%")
	.attr("y1", "0%")
	.attr("x2", "0%")
	.attr("y2", "100%");

	gradient.append("stop")
	.attr("offset", "0%")
	.attr("stop-color", minColor)
	.attr("stop-opacity", 1);

	gradient.append("stop")
	.attr("offset", "50%")
	.attr("stop-color", midColor)
	.attr("stop-opacity", 1);

	gradient.append("stop")
	.attr("offset", "100%")
	.attr("stop-color", maxColor)
	.attr("stop-opacity", 1);

	svg.append("rect")
	.attr("width", 20)
	.attr("height", 300)
	.style("fill", "url(#gradient)");

	svg.append("text")
	.attr("x", "25")
	.attr("y", "12")
	.attr("class","scale")
	.style("font-size","12px")
	.style("fill", "black")
	.text(roundNumber(min)+"%"); 

	svg.append("text")
	.attr("x", "25")
	.attr("y", "150")
	.attr("class","scale")
	.style("font-size","12px")
	.style("fill", "black")
	.text(roundNumber(mid)+"%");  

	svg.append("text")
	.attr("x", "25")
	.attr("y", "300")
	.attr("class","scale")
	.style("font-size","12px")
	.style("fill", "black")
	.text(roundNumber(max)+"%");   	  	
}

function percentChange(d,firstYear,secondYear)
{
	if(d.data == undefined || d.data[firstYear] == undefined || d.data[secondYear]== undefined)
	{
		return undefined;
	}
	
	return (((d.data[secondYear]-d.data[firstYear])/d.data[secondYear])*100);
}


function roundNumber(string)
{
	return string.toFixed(2);
}

//function that adds commas to number 
function numberToString(string)
{
	return string.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//Prototype that moves selected item to front 
d3.selection.prototype.moveToFront = function() {
	return this.each(function(){
		this.parentNode.appendChild(this);
	});
};



